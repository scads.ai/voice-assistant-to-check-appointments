#!/usr/bin/env python3

from vosk import Model, KaldiRecognizer
import pyaudio
import pyttsx3
from time import sleep
import datetime as dt
import json
import requests
from ics import Calendar, Event
from datetime import datetime
from datetime import timedelta
from datetime import timezone 
import subprocess

# configuration link to ics file
configuration_link = None

# text to speech
keyword = 'mister bond'
shortened_keyword = 'mr bond'

remembered_event = None
#assistant = pyttsx3.init(driverName='espeak')
assistant = pyttsx3.init()

# speech to text
path_to_speech_model = 'model_en'   # download a model from https://alphacephei.com/vosk/models
model = Model(path_to_speech_model)
kaldiRecognizer = KaldiRecognizer(model, 16000)

# start listening to microphone
portAudio = pyaudio.PyAudio()
stream = portAudio.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=8000)
stream.start_stream()


def recognize_voice(recognizer):
    while True:
        if stream.is_active():
            data = stream.read(4000)
            if recognizer.AcceptWaveform(data):
                result = json.loads(recognizer.FinalResult())['text']
                print(result)
                return result


def speak(text, pause=0.5):
    stream.stop_stream()
    assistant.say(text)
    assistant.runAndWait()
    sleep(pause)
    stream.start_stream()


def tell_time(result):
    if 'how late' in result:
        now = dt.datetime.now()
        hour = now.strftime('%H')
        minute = now.strftime('%M')
        speak('It is ' + hour + ' ' + minute)

def dayStart(timestamp):
    return timestamp.replace(hour=0, minute=0, second=0, microsecond=0)

def filterEventBetween(event, fromDate, toDate):
    return fromDate < event and event < toDate

def toHourAndMinute(timestamp):
    return timestamp.strftime("%H:%M")

def filterEventListBetween(eventList, fromTimestamp, toTimestamp):
    return sorted(filter(lambda event: filterEventBetween(event.begin, fromTimestamp, toTimestamp), eventList), key=lambda event: event.begin) 

def printEventListBetween(eventList, fromTimestamp, toTimestamp):
    events = filterEventListBetween(eventList, fromTimestamp, toTimestamp)
    if events:
        rememberNextEvent(events)
        for event in events:
            print(
"""%s to %s
Location: %s
Title: %s""" % (toHourAndMinute(event.begin), toHourAndMinute(event.end), event.location, event.name))
        return "Yes. Here are your appointments. The next is at %s %s."%(remembered_event.begin.hour, remembered_event.begin.minute)
    else:
        return "There are no appointments."

def rememberNextEvent(eventList):
    global remembered_event
    remembered_event = eventList[0]

def printLocation(eventList):
    now = datetime.now(timezone.utc)
    if remembered_event is None or remembered_event.begin < now:
        events = filterEventListBetween(eventList, now, dayStart(now + timedelta(days=100)))
        rememberNextEvent(events)
    print("Location: %s"%remembered_event.location)
    if remembered_event.location.startswith("https://"):
        return "A link is given."
    return "The location for the next event is " + remembered_event.location

def openBrowser(eventList):
    now = datetime.now(timezone.utc)
    if remembered_event is None or remembered_event.begin < now:
        events = filterEventListBetween(eventList, now, dayStart(now + timedelta(days=1)))
        rememberNextEvent(events)
    print("Location: %s"%remembered_event.location)
    url = remembered_event.location
    subprocess.Popen(["chromium-browser","--incognito", url],stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return "Opening browser for you"

def getCalendar():
    appointment_response = requests.get(configuration_link)
    if appointment_response.ok:
        return Calendar(appointment_response.text.replace("\x00",""))
    return None

def processCommand(command):
    if "appointments" in command or "appointment" in command:
        cal_data = getCalendar()
        if not cal_data:
            return "I cannot read your appointments"
        now = datetime.now(timezone.utc)
        weekday = now.weekday()
        if "today" in command:
            print("appointments today")
            return printEventListBetween(cal_data.events, now, dayStart(now + timedelta(days=1)))
        if "tomorrow" in command:
            print("appointments tomorrow")
            return printEventListBetween(cal_data.events, dayStart(now + timedelta(days=1)), dayStart(now + timedelta(days=2)))
        if "next week" in command:
            print("appointments next week")
            return printEventListBetween(cal_data.events, dayStart(now + timedelta(days=(7-weekday))), dayStart(now + timedelta(days=(14-weekday))))
        if "this week" in command:
            print("appointments this week")
            return printEventListBetween(cal_data.events, now, dayStart(now + timedelta(days=(7-weekday))))
    if "where" in command:
        cal_data = getCalendar()
        if not cal_data:
            return "I cannot read your appointments"
        return printLocation(cal_data.events)
    if ("open" in command and "browser" in command) or ("on the screen" in command):
        cal_data = getCalendar()
        if not cal_data:
            return "I cannot read your appointments"
        return openBrowser(cal_data.events)

    return "I don't know!"


def start_assistant(rate=165, voice_id='english'):
    global configuration_link
    with open("configuration.link") as f:
        configuration_link = f.readline()
    if configuration_link is None:
        raise ValueError("No link in configuration.link")
    assistant.setProperty('rate', rate)
    assistant.setProperty('voice', voice_id)

    print(keyword + ' has booted. It uses configuration link: ' + str(configuration_link))
    speak("""It's Bond, James Bond.""")

    print('rate', assistant.getProperty('rate'))
    print('voice', assistant.getProperty('voice'))
    while True:
        result = recognize_voice(kaldiRecognizer)
        if result.startswith("goodbye") or result.startswith("good bye"):
            speak("bye")
            break
        if result.startswith(keyword) or result.startswith(shortened_keyword) or result.startswith("james"):
            appointments_text = processCommand(result)
            speak(appointments_text)
#        if keyword in result:
#            tell_time(result)


start_assistant()

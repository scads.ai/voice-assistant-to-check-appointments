# Voice Assistant

This project contains the source code for Jan Frenzel's voice assistant programmed at the [hackathon at ScaDS.AI seminar trip 2022](https://git.informatik.uni-leipzig.de/scads/seminar-trip-hackathon). It can be used to check your appointments using voice commands. Use keyword `James` to start a command.

Commands that are understood:

- James, appointments today
- James, appointments tomorrow
- James, appointments this week
- James, appointments next week
- James, where
- James, on the screen!
- Goodbye, Mr. Bond!

Longer versions of these commands may also work. Try, for example: "James, show my appointments today" or "James, do I have appointments today?".

## Installation
A recent version of Python is required (tested with 3.8.0). If you have Ubuntu 18.04 LTS, you might want to install it using:

```bash
sudo apt-get install python3.8-dev
sudo apt-get install python3.8-env
```

If you installed packages `python3.8-*`, replace the first appearance of `python` below with `python3.8`.

The following steps install everything that is required:

```bash
sudo apt-get install portaudio19-dev
sudo apt-get install libespeak-dev
python -m venv myenv  # or: python3.8 -m venv myenv, see above
. myenv/bin/activate
pip install -U pip
pip install vosk
pip install pyaudio
pip install pyttsx3
pip install ics
```

**After that**, put a ICS link in the file `configuration.link`. You can generate such a link at https://msx.tu-dresden.de/owa/#path=/options/calendarpublishing.

## Usage
The program should start to talk after you start it using:

```bash
. myenv/bin/activate
make
```

## Support
The hope is that this README.md is self-explanatory. Please open issues if you feel that an improvement is necessary.

## Contributing
Contributions are very welcome! Feel free to share ideas by opening issues or contributing code.

## License
Please see [the License](LICENSE).

## Project status
This project is active, but the maintainer has a lot of other things to do. Thus, improvements (bug fixes, new features) only appear from time to time. So, please contribute!
